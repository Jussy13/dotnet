﻿using System;
using API.Models;
using API.Services;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("email/message/[action]")]
    public class HomeController : Controller
    {
        private readonly IMessageSaver _messageSaver;
        
        public HomeController(IMessageSaver messageSaver)
        {
            _messageSaver = messageSaver;
        }
        
        [ActionName("send")]
        [HttpPost]
        public IActionResult SendMessage([FromBody] Message message)
        {
            _messageSaver.SaveMessage(message);
            
            return new OkObjectResult("200");
        }

        [ActionName("read")]
        [HttpGet]
        public IActionResult ReadMessage([FromQuery] int id)
        {
            Message message = _messageSaver.GetMessage(id);
            
            return message == null ? new NotFoundObjectResult("Message not found") : new ObjectResult(message);
        }
        
        [ActionName("update")]
        [HttpPut]
        public IActionResult UpdateMessage([FromBody] Message message)
        {
            try
            {
                _messageSaver.UpdateMessage(message);
            }
            catch (NullReferenceException e)
            {
                return new NotFoundObjectResult("Message not found");
            }
            
            return new OkObjectResult("200");
        }
        
        [ActionName("delete")]
        [HttpDelete]
        public IActionResult DeleteMessage([FromQuery] int id)
        {
            try
            {
                _messageSaver.DeleteMessage(id);
            }
            catch (NullReferenceException e)
            {
                return new NotFoundObjectResult("Message not found");
            }
            
            return new OkObjectResult("200");
        }
    }
}