﻿using API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace API.Services
{
    public sealed class ApplicationContext : DbContext
    {
        public DbSet<MessageInDb> Messages { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseNpgsql(Startup.Configuration.GetConnectionString("Connection"));
        }
    }
}