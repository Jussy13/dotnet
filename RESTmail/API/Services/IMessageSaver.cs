using System;
using System.Linq;
using API.Models;

namespace API.Services
{
    public interface IMessageSaver
    {
        void SaveMessage(Message message);
        
        Message GetMessage(int id);
        
        void UpdateMessage(Message message);
        
        void DeleteMessage(int id);
    }
    
    public class MessageSaver : IMessageSaver 
    {
        public void SaveMessage(Message message)
        {
            var messageToSave = new MessageInDb()
            {
                Id = message.Id,
                Text = message.Text
            };

            using (var db = new ApplicationContext())
            {
                db.Messages.Add(messageToSave);
                db.SaveChanges();
            }
        }

        public Message GetMessage(int id)
        {
            MessageInDb message;
            using (var db = new ApplicationContext())
            {
                message = db.Messages.SingleOrDefault(j => j.Id == id);
            }

            if (message == null)
            {
                return null;
            }
            
            var messageToGet = new Message()
            {
                Id = message.Id,
                Text = message.Text
            };

            return messageToGet;
        }

        public void UpdateMessage(Message message)
        {
            MessageInDb mes;
            using (var db = new ApplicationContext())
            {
                mes = db.Messages.SingleOrDefault(j => j.Id == message.Id);
                
                if (message == null)
                {
                    throw new NullReferenceException();
                }

                mes.Text = message.Text;
                db.SaveChanges();
            }
        }

        public void DeleteMessage(int id)
        {
            MessageInDb message;
            using (var db = new ApplicationContext())
            {
                message = db.Messages.SingleOrDefault(j => j.Id == id);
                
                if (message == null)
                {
                    throw new NullReferenceException();
                }

                db.Messages.Remove(message);
                db.SaveChanges();
            }
        }
    }
}