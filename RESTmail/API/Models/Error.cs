using Newtonsoft.Json;

namespace API.Models
{
    public class Error
    {
        [JsonProperty("message")]
        public string Message { get; set; }
    }
}