using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace API.Models
{
    public class Message
    {
        [JsonProperty("id")]
        [Range(0.0, int.MaxValue, ErrorMessage = "Некорректно задан id сообщения")]
        public int Id { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }
    }
}